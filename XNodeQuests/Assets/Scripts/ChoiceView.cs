using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using XNode;

public class ChoiceView : MonoBehaviour
{
    public event Action<Node> OnClickChoice;
    [SerializeField] private TextMeshProUGUI choice;
    [SerializeField] private Button button;

    private void Start()
    {
        button.onClick.AddListener(OnClick);
    }

    private ChoiceElement _currentChoiceElement;
    
    public void SetChoice(ChoiceElement currentChoiceElement)
    {
        _currentChoiceElement = currentChoiceElement;
        UpdateUI();
    }

    private void UpdateUI()
    {
        choice.text = _currentChoiceElement.Message;
    }

    private void OnClick()
    {
        OnClickChoice?.Invoke(_currentChoiceElement.Node);
    }
}
