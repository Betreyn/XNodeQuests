using UnityEngine;

namespace CharacterSystem
{
    [CreateAssetMenu(fileName = "NewCharacterSettings", menuName = "Character", order = 0)]
    public class CharacterSettings : ScriptableObject
    {
        [SerializeField] private string characterName;

        public string CharacterName => characterName;

        [SerializeField] private Color characterColor;

        public Color CharacterColor => characterColor;
    }
}