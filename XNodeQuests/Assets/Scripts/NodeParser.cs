using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XNode;

public class NodeParser : MonoBehaviour
{
    public DialogueGraph graph;
    [SerializeField] private DialogueView dialogueView;
    [SerializeField] private AnswerOptions answerOptions;
    [SerializeField] private Button restartBtn;
    
    
    private void Start()
    {
        restartBtn.onClick.AddListener(RestartDialog);
        SetNode(graph.nodes[0]);
    }

    public void NextDialog()
    {
        if (graph.current == null)
        {
            return;
        }
        
        SetDialog(graph.current);
        
        var dNode = (DialogueNode)graph.current;
        
        if (dNode.choice.Length == 0)
        {
            answerOptions.Hide();
            if (graph.current.GetPort("_output").Connection != null)
            {
                graph.current = graph.current.GetPort("_output").Connection.node;
            }
        }
        else
        {
            GenerateChoices(dNode);
        }
    }
    
    private void GenerateChoices(DialogueNode node)
    {
        var choiceElements = new List<ChoiceElement>();
        for (int i = 0; i < node.choice.Length; i++)
        {
            Node getNode = node.GetPort($"choice {i}").Connection != null ? node.GetPort($"choice {i}").Connection.node : null;
            choiceElements.Add(new ChoiceElement(getNode, node.choice[i]));
        }

        answerOptions.OnChoiceSelected += ChoiceSelected;
        answerOptions.GenerateChoices(choiceElements);
    }

    private void ChoiceSelected(Node node)
    {
        answerOptions.OnChoiceSelected -= ChoiceSelected;
        answerOptions.Hide();
        graph.current = node;
        NextDialog();
    }
    
    private void SetNode(Node node)
    {
        graph.current = node;
        
        NextDialog();
        
    }

    private void SetDialog(Node node)
    {
        switch (node)
        {
            case DialogueNode dialogue:
            {
                dialogueView.SetDialog(dialogue.Character.CharacterName, dialogue.dialogueLine);
                dialogueView.SetColor(dialogue.Character.CharacterColor);
                break;
            }
        }
    }

    private void RestartDialog()
    {
        SetNode(graph.nodes[0]);
    }
}
