using System;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class AnswerOptions : MonoBehaviour
{
    public event Action<Node> OnChoiceSelected; 
    [SerializeField] private ChoiceView[] choiceViews;

    private void OnEnable()
    {
        for (int i = 0; i < choiceViews.Length; i++)
        {
            choiceViews[i].OnClickChoice += CallbackChoice;
        }
    }

    private void OnDisable()
    {
        for (int i = 0; i < choiceViews.Length; i++)
        {
            choiceViews[i].OnClickChoice -= CallbackChoice;
        }
    }

    public void GenerateChoices(List<ChoiceElement> choices)
    {
        for (int i = 0; i < choiceViews.Length; i++)
        {
            if (i < choices.Count)
            {
                choiceViews[i].gameObject.SetActive(true);
                choiceViews[i].SetChoice(choices[i]);
            }
            else
            {
                break;
            }
        }
    }

    public void Hide()
    {
        for (int i = 0; i < choiceViews.Length; i++)
        {
            choiceViews[i].gameObject.SetActive(false);
        }
    }

    private void CallbackChoice(Node node)
    {
        OnChoiceSelected?.Invoke(node);
    }
    
    
}
