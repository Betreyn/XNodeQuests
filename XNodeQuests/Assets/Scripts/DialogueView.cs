using System;
using TMPro;
using UnityEngine;

public class DialogueView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI speakerText;
    [SerializeField] private TextMeshProUGUI dialogueText;

    private Color _defaultColor;

    private void Start()
    {
        _defaultColor = speakerText.color;
    }

    public void SetDialog(string speaker, string dialogue)
    {
        speakerText.text = speaker;
        dialogueText.text = dialogue;
    }

    public void SetColor(Color color)
    {
        speakerText.color = color;
    }

    public void Hide()
    {
        speakerText.text = "";
        dialogueText.text = "";
        speakerText.color = _defaultColor;
    }
}
