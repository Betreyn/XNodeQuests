using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class ChoiceElement
{
    private Node _node;
    public Node Node => _node;

    private string _message;
    public string Message => _message;

    public ChoiceElement(Node node, string message)
    {
        _node = node;
        _message = message;
    }
    
}
