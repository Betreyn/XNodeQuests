﻿using CharacterSystem;
using UnityEngine;
using XNode;

public class DialogueNode : Node
{

	[Input] public int _input;
	[Output] public int _output;

	[Output(dynamicPortList = true)]
	[TextArea]
	public string[] choice;

	[SerializeField] private CharacterSettings character;

	public CharacterSettings Character => character;

	[TextArea]
	public string dialogueLine;
	
}