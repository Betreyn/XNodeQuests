﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using XNode;

[CreateAssetMenu]
public class DialogueGraph : NodeGraph
{
	public Node current;
}